from redbot.core.bot import Red

from .psoalert import PsoAlert

def setup(bot):
    n = PsoAlert(bot)
    bot.add_listener(n.echo_message, "on_message")
    bot.add_cog(n)
