import discord
from redbot.core import commands
from random import shuffle
MSG_HEADER = '**'
TEAM_HEADER = '__**Team {0}:**__\n'
IMBALANCE_WARNING = '**Warning: Teams are unbalanced.**\n\n'
INVALID_NUM_ERROR = '**Error: There are more teams than members available**'
class Teams(commands.Cog):
    """Generate random teams"""
    def __init__(self, bot):
        self.bot = bot

    def create_team_message(self, team_num, team_members):
        message = TEAM_HEADER.format(team_num)
        for member in team_members:
            message += '{0}\n'.format(member)
        message += '\n'
        return message


    @commands.command(pass_context=True)
    async def teams(self, ctx, num_teams : int, *members : str):
        """"""
        if len(members) < num_teams:
            await self.bot.say(INVALID_NUM_ERROR)
            return

        message = ''

        # number of teams that will get extra members
        leftover_members = len(members) % num_teams
        members_per_team = len(members) // num_teams

        # shuffle members
        members = list(members)
        shuffle(members)

        if leftover_members > 0:
            message += IMBALANCE_WARNING

        index = 0
        team_index = 0
        while team_index < num_teams:
            # endpoint index for member list
            new_index = index + members_per_team

            # teams with less members will be first in the team list
            if num_teams - team_index <= leftover_members:
                # add an additional member to fill out teams
                new_index += 1

            # get team members for team
            team = members[index:new_index]

            message += self.create_team_message(team_index+1, team)
            index = new_index
            team_index += 1

        await ctx.send(message)

