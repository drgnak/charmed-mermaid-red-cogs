import discord
from redbot.core import commands
from redbot.core import Config
from mechanicalsoup import Browser
import re


class PsoAlert(commands.Cog):
    """Echo alerts from a channel"""
    #TODO: Cache config data and remove registered_users
    def __init__(self, bot):
        self.FRESH_FINDS_URI = 'https://github.com/SynthSy/PSO2-Dictionary/wiki/Fresh-Finds'
        self.browser = Browser()
        self.bot = bot
        self.config = Config.get_conf(self, identifier=731631630053015645)
        self.DELETE_AFTER = 300
        self.bun_words = ['rabbit', 'bunny']
        self.defaults = {
            'subscriptions': {
                'enabled': False,
                'preurgent': False,
                'urgent': False,
                'fresh': False,
                'weekly': False,
                'daily': False,
                'casino': False
            },
            'subscription_autodelete': {
                'autodelete': False,
                'preurgent': 1800,
                'urgent': 1800,
                'fresh': 86400,
                'weekly': 604800,
                'daily': 86400,
            }
        }
        self.default_globals = {
            'bunny_alert': None
        }
        self.config.register_user(**self.defaults)
        self.config.register_channel(**self.defaults)
        self.config.register_global(**self.default_globals)

    @commands.command()
    async def test(self, ctx):
        embed = discord.Embed.from_dict({
            "thumbnail": {
                "width": 296,
                "url": "https://cdn.arks-layer.com/uqimages/5sduU11.png",
                "proxy_url": "https://images-ext-1.discordapp.net/external/tuSGCUffDfKL0YF_wzxpk8KEvP02qeEu4RPnjqv2Ko8/https/cdn.arks-layer.com/uqimages/5sduU11.png",
                "height": 299
            },
            "fields": [
                {
                    "value": "[The Fresh Finds Preview has been updated! Click here to access it.](https://github.com/SynthSy/PSO2-Dictionary/wiki/Fresh-Finds)",
                    "name": "Link",
                    "inline": False
                }
            ],
            "color": 16777214,
            "type": "rich",
            "title": "Fresh Finds Updated!"
        })
        await ctx.send(embed=embed)

    @commands.command()
    async def test2(self, ctx, messageid):
        message = await ctx.channel.fetch_message(messageid)
        try:
            embed = message.embeds[0]
            print(str(embed.to_dict()))
            await ctx.send(embed=embed)
        except:
            await ctx.send("error occurred")
    @commands.command()
    async def testtype(self, ctx, messageid):
        message = await ctx.channel.fetch_message(messageid)
        try:
            embed = message.embeds[0]
            print(str(embed.to_dict()))
            await ctx.send(self.is_valid_alert(embed, None))
        except:
            await ctx.send("error occurred")
    @commands.command()
    async def test3(self, ctx, messageid):
        message = await ctx.channel.fetch_message(messageid)
        try:
            await ctx.send(message.attachments[0].url)
            print(str(message.attachments))
        except Exception as e:
            print(e)
            print(message.attachments)
            await ctx.send("error occurred")
    @commands.group(name='psoalert', pass_context=True)
    async def _psoalert(self, ctx):
        """PSO Alert stuff"""
        if ctx.invoked_subcommand is None:
            return

    async def set_user_subscription(self, ctx, alert_type, value):
        await self.config.user(ctx.author).subscriptions.set_raw(alert_type, value=value)

    async def set_channel_subscription(self, ctx, alert_type, value):
        await self.config.channel(ctx.channel).subscriptions.set_raw(alert_type, value=value)

    async def set_channel_setting(self, ctx, alert_type, value):
        await self.config.channel(ctx.channel).subscription_autodelete.set_raw(alert_type, value=value)

    @_psoalert.command(name="bunny")
    async def psoalert_bunny(self, ctx):
        user_id = ctx.author.id
        await self.config.bunny_alert.set(user_id)
        await ctx.send("Done")

    @_psoalert.command(name="bunny_test")
    async def psoalert_bunny_test(self, ctx):
        test_string = "The Bunny: 123456"

        user_id = await self.config.bunny_alert()
        message_lower = test_string.lower()
        if user_id and any(word in message_lower for word in self.bun_words):
            await ctx.send("<@{}>".format(user_id))
        else:
            await ctx.send("No user ID found")

    @_psoalert.group(name="channel")
    async def _channel(self, ctx):
        """
        Manage subscriptions for a channel
        """
        return

    @_channel.command(name="subscribe")
    async def channel_subscribe(self, ctx):
        """
        Add this channel to PSO Alert notifications
        """
        await self.set_channel_subscription(ctx, 'enabled', True)
        await ctx.send("This channel is now subscribed to PSO2NA notifications.", delete_after=60)

    @_channel.command(name="unsubscribe")
    async def channel_unsubscribe(self, ctx):
        """
        Remove this channel from PSO Alert notifications
        """
        
        await self.config.channel(ctx.channel).clear()
        await ctx.send("This channel is no longer subscribed to PSO2NA notifications.", delete_after=60)
    
    @_channel.command(name="subscriptions")
    async def channel_subscriptions(self, ctx):
        """Get the alerts this channel are subscribed to"""
        subscriptions = await self.config.channel(ctx.channel).subscriptions()
        await ctx.send(str(subscriptions), delete_after=60)


    @_channel.command()
    async def set(self, ctx, alert_type, value):
        if alert_type == 'autodelete':
            if value == 'True':
                value = True
            else:
                value = False
        else:
            value = int(value)
        await self.set_channel_setting(ctx, alert_type, value)
        await ctx.send('Done', delete_after=60)

    @_channel.command()
    async def get(self, ctx):
        subscriptions = await self.config.channel(ctx.channel).subscription_autodelete()
        await ctx.send(str(subscriptions), delete_after=60)

    @_psoalert.command(name="subscribe")
    async def user_subscribe(self, ctx, alert_type):
        """
        Add yourself to PSO Alert notifications
        
        Valid types:
        **all** Subscribe to all alerts
        **preurgent** UQ 15/30 minutes before they happen
        **urgent** UQ when they start
        **fresh** New fresh finds
        **weekly** Alerts about weekly quests
        **daily** Daily quest reset
        """
        if alert_type == 'all':
            for key in self.defaults['subscriptions']:
                await self.set_user_subscription(ctx, key, True)
            await ctx.author.send("You are now subscribed to all PSO2NA notifications.")
        elif alert_type not in self.defaults['subscriptions']:
            await ctx.send("`{}` is not a valid alert type.".format(alert_type), delete_after=60)
        else:
            await self.set_user_subscription(ctx, alert_type, True)
            await ctx.author.send("You are now subscribed to PSO2NA {} notifications.".format(alert_type))

    @_psoalert.command(name="unsubscribe")
    async def user_unsubscribe(self, ctx, alert_type):
        """
        Remove yourself from PSO Alert notifications
        
        Valid types:
        **all** Unsubscribe from all alerts
        **preurgent** UQ 15/30 minutes before they happen
        **urgent** UQ when they start
        **fresh** New fresh finds
        **weekly** Alerts about weekly quests
        **daily** Daily quest reset
        """
        if alert_type == 'all':
            await self.config.user(ctx.author).clear()
            await ctx.author.send("You are no longer subscribed to PSO2NA notifications.")
        elif alert_type not in self.defaults['subscriptions']:
            await ctx.send("`{}` is not a valid alert type.".format(alert_type), delete_after=60)
        else:
            await self.set_user_subscription(ctx, alert_type, False)
            await ctx.author.send("You are no longer subscribed to PSO2NA {} notifications.".format(alert_type))

    @_psoalert.command(name="subscriptions")
    async def user_subscriptions(self, ctx):
        """Get the alerts you are subscribed to"""
        subscriptions = await self.config.user(ctx.author).subscriptions()
        await ctx.send(str(subscriptions), delete_after=60)

    def is_valid_alert(self, embed, config):
        try:
            description = embed.description
            title = embed.title
            if description:
                if re.search('urgent quest is happening in', description):
                    return 'preurgent'
                elif re.search('urgent quest is happening now', description):
                    return 'urgent'
                elif re.search('Daily missions/orders will reset', description):
                    return 'daily'
                elif re.search('SG Scratch', description):
                    return 'daily'
                elif re.search('Weekly', description):
                    return 'weekly'
                elif re.search('casino', description):
                    return 'casino'
                elif re.search('Maintenance', description):
                    return 'maintenance'
            if title:
                if re.search('Fresh Finds', title):
                    return 'fresh'
                elif re.search('New Daily', title):
                    return 'daily'
        except:
            pass

        return 'dunno'
    
    def get_fresh_finds(self):
        """Get today's fresh finds"""
        # Get the table from the fresh finds github page
        page = self.browser.get(self.FRESH_FINDS_URI)
        fresh_finds_table = page.soup.find_all('th', text=re.compile('Official Name'))[0].parent.parent.parent
        
        # Get date for fresh finds
        fresh_finds_title = 'Fresh Finds For ' + fresh_finds_table.previous_sibling.previous_sibling.contents[-1]
        fresh_finds_footer = 'https://github.com/SynthSy/PSO2-Dictionary/wiki/Fresh-Finds'
        # Get fresh finds from table
        fresh_finds = fresh_finds_table.find_all('tbody')[0]
        fresh_finds_description = ''
        for fresh_find in fresh_finds.children:
            try:
                # Ignore blank lines
                if (fresh_find == '\n'):
                    continue
                # Append newline after the first fresh find
                if len(fresh_finds_description) != 0:
                    fresh_finds_description += '\n'

                # Extract data from table columns 
                columns = fresh_find.find_all('td')
                name = columns[0].contents[0]
                cost = columns[4].contents[0]
                url = columns[5].find_all('a')[0]['href']
                item_type = columns[3].contents[0]

                # Append line
                fresh_finds_description += '{cost} :moneybag: [**{name}**]({url}) ({item_type})'.format(name=name, cost=cost, url=url, item_type=item_type)
            except AttributeError:
                # Don't care about empty children
                pass
        has_bunny = False
        description_lower = fresh_finds_description.lower()
        if any(word in description_lower for word in self.bun_words):
            has_bunny = True

        # output = {'title':fresh_finds_title, 'description':fresh_finds_description}
        # print(output)
        # print(fresh_finds_description)

        embed = discord.Embed(title=fresh_finds_title, 
                        description=fresh_finds_description)
        embed.set_footer(text=fresh_finds_footer)
        return embed, has_bunny

    @_psoalert.command()
    async def fresh(self, ctx):
        embed, has_bunny = self.get_fresh_finds()
        user_id = await self.config.bunny_alert()
        await ctx.send(embed=embed, delete_after=60)
        if user_id and has_bunny:
            await ctx.send("<@{}>".format(user_id))

    async def echo_message(self, ctx):
        #if ctx.channel.id == 731631630053015645 and ctx.author.id != 462108927711248384:
        if ctx.channel.id == 731631630053015645:
            if len(ctx.embeds) > 0:
                embed = ctx.embeds[0]
                try:
                    embed.description = embed.description.replace('Cur_Meseta', 'gem')
                except AttributeError:
                    pass
                all_users = await self.config.all_users()
                for userid in all_users:
                    user = self.bot.get_user(userid)
                    config = all_users[userid]['subscriptions']
                    if self.is_valid_alert(embed, config):
                        try:
                            await user.send(ctx.content, embed=embed)
                        except AttributeError:
                            pass
                        print("Sent thing")
            else:
                embed = None

            if len(ctx.attachments) > 0:
                message = ctx.attachments[0].url
                print("found attachment")
            else:
                message = ''

            if (message or embed):
                all_channels = await self.config.all_channels()
                for channelid in all_channels:
                    found_bunny = False
                    channel = self.bot.get_channel(channelid)
                    config = all_channels[channelid]['subscriptions']
                    autodelete = all_channels[channelid]['subscription_autodelete']
                    notification_type = self.is_valid_alert(embed, config)
                    if notification_type == 'fresh':
                        embed, has_bunny = self.get_fresh_finds()
                        if has_bunny:
                            found_bunny = True

                    # message = ctx.content
                    delete_after = None
                    if 'autodelete' in autodelete and autodelete['autodelete']:
                        print("Message type " + notification_type)
                        try:
                            delete_after = autodelete[notification_type]
                        except KeyError:
                            try:
                                delete_after = self.defaults['subscription_autodelete'][notification_type]
                            except KeyError:
                                #message = "I don't know anything about these notifications"
                                if len(message) == 0:
                                    delete_after = 1800
                    
                    await channel.send(message, embed=embed, delete_after=delete_after)
                    if found_bunny:
                        user_id = await self.config.bunny_alert()
                        if user_id:
                            await channel.send("<@{}>".format(user_id), delete_after=delete_after)
                    print("Sent channel")
                #elif re.search('', description):
            #await channel.send("hello")
