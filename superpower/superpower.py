import discord
from redbot.core import commands
from mechanicalsoup import Browser
from datetime import datetime
import logging
BASE_URL = 'http://powerlisting.wikia.com/wiki/'
URL = 'http://powerlisting.wikia.com/wiki/Special:Random'
SUPERPOWER_RESPONSE = '{}\n{}:\n{}'
DAILY_RESPONSE = '{}\nSuperpowers for {}:\n```{}```'
class Superpower(commands.Cog):
    """My custom cog that does stuff!"""

    def __init__(self, bot):
        self.bot = bot
        self.browser = Browser()
        self.cached_powers = {}
        self.today = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)
        logging.debug(self.today)

    @commands.group(name="superpower", invoke_without_command=True, pass_context=True)
    async def _superpower(self, ctx):
        """This does stuff!"""
        if ctx.invoked_subcommand is None:
            author = ctx.message.author

            # Check if today is a new day so the user can get a new power
            self.check_date()
            try:
                power, url = self.cached_powers[author.name]
                logging.debug('Cached power found: ' + power)
            except KeyError:
                logging.debug('No cached powers found. Looking for a random power.')
                power, url = self.random_superpower()
                # Cache user's power for today
                self.cached_powers[author.name] = (power, url)
                logging.debug(self.cached_powers)
            response = SUPERPOWER_RESPONSE.format(author.mention, 'Your power for ' + str(self.today).split()[0], url)
            #Your code will go here
            await ctx.send(response)
        else:
            print(args)

    @_superpower.command(pass_context=True)
    async def random(self, ctx):
        """Get a random superpower"""
        author = ctx.message.author
        power, url = self.random_superpower()
        await ctx.send('' + author.mention + '\n' + url)

    @_superpower.command(pass_context=True)
    async def listdaily(self, ctx):
        self.check_date();
        author = ctx.message.author
        message = ''
        i = 0
        for key, value in self.cached_powers.items():
            i += 1
            message += str(i) + ': ' + key + ' - ' + value[0] + '\n' + value[1] + '\n\n'

        if message:
            await ctx.send(DAILY_RESPONSE.format(author.mention, str(self.today).split()[0], message))
        else:
            await ctx.send('{}\nThere are no users with superpowers for today.'.format(author.mention))

    def check_date(self):
        """Check if it's the new day and clear the list if so"""
        if (datetime.today() - self.today).days != 0:
            self.cached_powers.clear()
            self.today = datetime.today().replace(hour=0, minute=0, second=0, microsecond=0)


    def random_superpower(self):
        """Get random superpower page and title text"""
        page = self.browser.get(URL)
        power = page.soup.title.get_text().split(' | ')[0]
        url = BASE_URL + power.replace(' ', '_')
        return (power, url)

