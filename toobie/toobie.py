import discord
from redbot.core import commands
from random import randint
from urllib.request import urlopen
from xml.etree import ElementTree

SEARCH_URL = 'http://safebooru.org/index.php?page=dapi&s=post&q=index&limit=1&pid={pid}&json=0&tags=yorha_no._2_type_b'
RESPONSE_TEXT = '2B #{post_num}:'
MAX_ENTRIES_DEFAULT = 2063

class Toobie(commands.Cog):
    """Plugin that lets the user fetch a random 2B image from Safebooru"""
    def __init__(self, bot):
        self.bot = bot
        self.update_max_entries()

    @commands.command(name='2b', invoke_without_command=True, pass_context=True)
    async def toobie(self, ctx, post_number : int=0):
        """Get a random 2B image from Safebooru"""
        #get post entry xml from safebooru api
        if post_number < 1:
            pid = randint(0, self.max_entries)
        else:
            self.update_max_entries()
            pid = self.max_entries - post_number

        root = self.get_post(pid)

        try:
            #Get the post number
            old_max_entries = self.max_entries
            self.max_entries = int(root.get('count'))
            offset = self.max_entries - old_max_entries
            post_num = offset + old_max_entries - pid

            #return the image url
            post = root.getchildren()[0]
            url = post.get('file_url')
            embed=discord.Embed(title=RESPONSE_TEXT.format(post_num = post_num), url=url)
            embed.set_image(url=url)
            await ctx.send(embed=embed)
        except Exception as e:
            print(e)
            await ctx.send("2B: Invalid post ID")

    def get_post(self, pid):
        response = urlopen(SEARCH_URL.format(pid=pid))
        if response:
            return ElementTree.parse(response).getroot()
        else:
            return None

    def update_max_entries(self):
        """Update max_entries with the latest post count"""
        root = self.get_post(1000000)
        self.max_entries = int(root.get('count'))
